# Kin Rai Dee! 🍔

เว็บแอพลิเคชั่น Kin Rai dee! ถูกคิดขึ้นมาสำหรับแก้ไขปัญหาคิดไม่ออกว่ามื้อนี้จะกินอะไรดี ไม่ว่าจะเป็นมื้อเที่ยงกับแก๊งค์เพื่อนๆที่ทำงาน หรืออยู่คนเดียวเหงาๆ นึกไม่ออกว่าจะกินอะไรดี สามารถหาร้านใกล้ๆรอบตัวคุณได้ง่ายๆด้วยฟีเจอร์ของเว็บแอพลิเคชั่น Kin Rai dee!

## Features

- สุ่มจาก Maps 🗺
- สุ่มจากเมนูที่คิดได้ 🤔
- สุ่มจากเมนู Basic 🍲
- รวมโปรดีๆ 🎉

## Screenshots

![App Screenshot](https://kin-rai-dee.vercel.app/app-screenshot.png)

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`MAPS_KEY`

Get key api for `MAPS_KEY` from this docs https://map.longdo.com/docs/

## Tech Stack

- Nextjs/Reactjs - Frontend Framework
- Material UI - Components UI Framework
- Vercel - Static Hosting Website

## Installation

Install with npm or yarn

npm

```bash
  npm install
  npm run dev
```

yarn

```bash
  yarn install
  yarn dev
```

## Demo

https://kin-rai-dee.vercel.app/

## Authors

- [@pimggwp](https://www.github.com/pimggwp) 🙇🏻‍♂️
