import React, { useState, useEffect } from "react";
import Head from "next/head";
import { FaArrowLeft } from "react-icons/fa";
import Link from "next/link";
import axios from "axios";
import LongdoMap, { longdo, map } from "./longdo-map/LongdoMap";
import {
  Grid,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

const randomByMaps = () => {
  const mapKey = process.env.MAPS_KEY;
  const [restaurantList, setRestaurantList] = useState([]);
  const [open, setOpen] = useState(false);
  const [randomDone, setRandomDone] = useState("");

  useEffect(() => {
    if (open === false) setRandomDone("");
  }, [open]);

  useEffect(() => {
    setRestaurantList([]);
  }, []);

  useEffect(() => {
    if (restaurantList.length > 0) {
      addMarkers(restaurantList);
    }
  }, [restaurantList]);

  function addMarker(lat, lon, name) {
    var marker = new longdo.Marker(
      { lon, lat },
      {
        popup: {
          html: `
            <div style="background: white; padding: 6px 14px; border-radius: 8px; width: 150px; box-shadow: 0 5px 25px 0 rgba(0, 0, 0, 0.1); z-index: 5;">
              <p>${name}</p>
            </div>
          `,
        },
      }
    );
    map.Overlays.add(marker);
  }

  function addMarkers(list) {
    if (list.length > 0) {
      list.forEach((el) => {
        addMarker(el.lat, el.lon, el.name);
      });
    }
  }

  async function initMap() {
    await map.Layers.setBase(longdo.Layers.GRAY);
    await map.Ui.Geolocation.visible(true);
    await map.location(longdo.LocationMode.Geolocation);
    await searchNearby();
    var xsum = 0;
    var ysum = 0;
    map.Event.bind("drop", function (event) {
      ysum = 0;
      xsum = 0;
      var result = map.location(longdo.LocationMode.Pointer);
      searchNearby(result.lat, result.lon);
      clearMarker();
    });
  }

  function clearMarker() {
    setRestaurantList([]);
    map.Overlays.clear();
  }

  async function searchNearby(lat = null, lon = null) {
    if (lat == null && lon == null) {
      const result = await map.location();
      lat = result.lat;
      lon = result.lon;
    }
    await axios
      .get(
        `https://api.longdo.com/POIService/json/search?key=${mapKey}&lat=${lat}&lon=${lon}&keyword=restaurant`
      )
      .then((res) => setRestaurantList(res.data.data));
  }

  async function randomNearMe() {
    await map.location(longdo.LocationMode.Geolocation);
    setTimeout(() => {
      const result = map.location();
      var lat = result.lat;
      var lon = result.lon;
      clearMarker();
      searchNearby(lat, lon);
      randomInList();
    }, 1000);
  }

  function randomInList() {
    setOpen(true);
    const random = Math.floor(Math.random() * restaurantList.length);
    setTimeout(() => {
      setRandomDone(restaurantList[random].name);
    }, 1000);
  }

  async function clickDetail(lat, lon, name) {
    var marker = new longdo.Marker({ lon: lon, lat: lat });
    await map.Overlays.clear();
    await map.Overlays.bounce(marker);
    setTimeout(async () => {
      await map.Overlays.bounce(null);
      addMarker(lat, lon, name);
    }, 2000);
  }

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Head>
        <title>Random!</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main style={{ width: "100%" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <h1 style={{ marginBottom: 5, marginRight: 16, cursor: "pointer" }}>
            <Link href="/">
              <FaArrowLeft />
            </Link>
          </h1>
          <h1 className="title">
            สุ่มจาก
            <span style={{ color: "#F79D19" }}> Maps</span>
          </h1>
        </div>
        <Grid container spacing={2}>
          <Grid item xs={8} md={8}>
            <div style={{ height: "calc(100vh - 110px)" }}>
              <LongdoMap id="longdo-map" mapKey={mapKey} callback={initMap} />
            </div>
          </Grid>
          <Grid item xs={4} md={4}>
            <div style={{ display: "flex" }}>
              <div
                style={{
                  maxHeight: "calc(100vh - 110px)",
                  width: "100%",
                }}
              >
                <Button
                  variant="contained"
                  fullWidth
                  onClick={() => randomNearMe()}
                  style={{ marginBottom: 8 }}
                >
                  สุ่มใกล้ฉัน
                </Button>
                <Button
                  variant="contained"
                  fullWidth
                  onClick={() => randomInList()}
                >
                  สุ่มในรายการ
                </Button>
                <div
                  style={{
                    maxHeight: "calc(100vh - 193px)",
                    width: "100%",
                    overflow: "scroll",
                  }}
                >
                  {restaurantList.map((el) => (
                    <div
                      onClick={() => clickDetail(el.lat, el.lon, el.name)}
                      style={{
                        background: "white",
                        padding: "20px",
                        border: "1px solid #eaeaea",
                        borderRadius: "10px",
                        marginTop: "10px",
                        marginBottom: "10px",
                        cursor: "pointer",
                      }}
                    >
                      {el.name}
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
        <Dialog
          open={open}
          onClose={() => {
            setOpen(false);
          }}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"ลองเป็นร้านนี้ดูไหม?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {randomDone ? (
                <div className="animate-bottom">
                  <h2>{randomDone}</h2>
                </div>
              ) : (
                <div className="loader"></div>
              )}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                setOpen(false);
              }}
              autoFocus
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </main>
      <style jsx>
        {`
          .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 80px;
            height: 80px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            margin: 0 auto;
          }

          .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
          }

          @-webkit-keyframes animatebottom {
            from {
              bottom: -100px;
              opacity: 0;
            }
            to {
              bottom: 0px;
              opacity: 1;
            }
          }

          @keyframes animatebottom {
            from {
              bottom: -100px;
              opacity: 0;
            }
            to {
              bottom: 0;
              opacity: 1;
            }
          }

          /* Safari */
          @-webkit-keyframes spin {
            0% {
              -webkit-transform: rotate(0deg);
            }
            100% {
              -webkit-transform: rotate(360deg);
            }
          }

          @keyframes spin {
            0% {
              transform: rotate(0deg);
            }
            100% {
              transform: rotate(360deg);
            }
          }
        `}
      </style>
    </div>
  );
};

export default randomByMaps;
