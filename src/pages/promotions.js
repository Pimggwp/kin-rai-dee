import React from "react";
import Head from "next/head";
import { FaArrowLeft } from "react-icons/fa";
import Link from "next/link";
import Image from "next/image";

const promotions = () => {
  return (
    <div
      style={{
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Head>
        <title>Promotions</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main style={{ width: "100%" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <h1 style={{ marginBottom: 5, marginRight: 16, cursor: "pointer" }}>
            <Link href="/">
              <FaArrowLeft />
            </Link>
          </h1>
          <h1 className="title">
            รวม <span style={{ color: "#F79D19" }}>Promotion</span> โดนๆ
          </h1>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            flexDirection: "column",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <p style={{ marginRight: 8 }}>สนับสนุนโดย</p>
            <Image src="/evo.png" alt="evo" width="150" height="90" />
          </div>
          <div style={{ maxWidth: "100%" }}>
            <Image
              style={{ margin: "0 auto;" }}
              src="/promo.jpg"
              alt="evo"
              width="450"
              height="450"
            />
            <Image
              style={{ margin: "0 auto;" }}
              src="/promo.jpg"
              alt="evo"
              width="450"
              height="450"
            />
            <Image
              style={{ margin: "0 auto;" }}
              src="/promo.jpg"
              alt="evo"
              width="450"
              height="450"
            />
          </div>
          <p>FYI: ข้อมูล Mockup เผื่อสปอนเซอร์เข้าในอนาคต 🥺 </p>
        </div>
      </main>
    </div>
  );
};

export default promotions;
