import Head from "next/head";
import Image from "next/image";

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Kin Rai Dee!</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="title">
          Welcome to <span style={{ color: "#0066FF" }}>Kin Rai Dee!</span>
        </h1>

        <div style={{ display: "flex", alignItems: "center", marginTop: 10 }}>
          <p className="description" style={{ marginRight: 8 }}>
            ร่วมสนุกไปกับ{" "}
          </p>
          <Image src="/evo.png" alt="evo" width="150" height="90" />
        </div>

        <div className="grid">
          <a href="/randomByMaps" className="card">
            <h3>สุ่มจาก Maps 🗺 &rarr;</h3>
            <p>แถวนี้มีอะไรอร่อยบ้างนะ สุ่มจาก Maps หาของกินอร่อยใกล้ๆดีกว่า</p>
          </a>

          <a href="/randomByMenuInput" className="card">
            <h3>สุ่มจากเมนูที่คิดได้ 🤔 &rarr;</h3>
            <p>
              มาเป็นแก๊งค์ ใครคิดอะไรออก แต่ไม่รู้จะเลือกอะไรดี
              กรอกเมนูแล้วสุ่มได้เลย
            </p>
          </a>

          <a href="/randomByBasicMenu" className="card">
            <h3>สุ่มจากเมนู Basic 🍲 &rarr;</h3>
            <p>รวมเมนูอาหารยอดฮิต หมดปัญหาแฟนบอกอะไรก็ได้ นึกไม่ออก สุ่มเลย!</p>
          </a>

          <a href="/promotions" className="card">
            <h3>รวมโปรดีๆ 🎉 &rarr;</h3>
            <p>ไม่พลาดโปรเด็ดร้านดัง ช่วงนี้มีอะไรบ้าง อย่าลืมเข้ามาเช็คดู</p>
          </a>
        </div>
      </main>

      <footer>
        <a
          href="https://www.instagram.com/pimggwp/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by pimggwp
        </a>
      </footer>

      <style jsx>{`
        .container {
          min-height: 100vh;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 2rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer img {
          margin-left: 0.5rem;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .title a {
          color: #0070f3;
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }

        .description {
          line-height: 1.5;
          font-size: 1.2rem;
        }

        code {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          font-size: 1.1rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }

        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }

        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }

        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }

        .logo {
          height: 1em;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
