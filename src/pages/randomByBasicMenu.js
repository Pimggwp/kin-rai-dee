import React, { useState, useEffect } from "react";
import Head from "next/head";
import { FaArrowLeft } from "react-icons/fa";
import Link from "next/link";
import { basicFood, foodCategory } from "../const/basicFood";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

const randomByBasicMenu = () => {
  const [open, setOpen] = useState(false);
  const [randomDone, setRandomDone] = useState("");
  const [randomType, setRandomType] = useState("");

  useEffect(() => {
    if (open === false) {
      setRandomDone("");
      setRandomType("");
    }
  }, [open]);

  function randomInList() {
    setOpen(true);
    const random = Math.floor(Math.random() * basicFood.length);
    setTimeout(() => {
      setRandomDone(basicFood[random].name);
      setRandomType(basicFood[random].cuisine);
    }, 1000);
  }

  return (
    <div
      style={{
        minHeight: "calc(100vh - 90px)",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Head>
        <title>Random!</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main style={{ width: "100%" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <h1 style={{ marginBottom: 5, marginRight: 16, cursor: "pointer" }}>
            <Link href="/">
              <FaArrowLeft />
            </Link>
          </h1>
          <h1 className="title">
            สุ่มจาก<span style={{ color: "#F79D19" }}>เมนู Basic</span>
          </h1>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            width: "100%",
          }}
        >
          <h3>รวมเมนูอาหารยอดฮิต หมดปัญหาแฟนบอกอะไรก็ได้ นึกไม่ออก สุ่มเลย!</h3>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "calc(100vh - 190px)",
          }}
        >
          <iframe
            src="https://giphy.com/embed/9C1nyePnovqlpEYFMD"
            width="200"
            height="200"
            style={{ position: "absolute" }}
            frameBorder="0"
            className="giphy-embed"
            allowFullScreen
          ></iframe>
          <p>
            <a href="https://giphy.com/gifs/justin-doge-dogecoin-9C1nyePnovqlpEYFMD">
              via GIPHY
            </a>
          </p>
          <Button
            variant="contained"
            onClick={() => randomInList()}
            style={{ position: "absolute" }}
          >
            สุ่มเลย!
          </Button>
        </div>
        <Dialog
          open={open}
          onClose={() => {
            setOpen(false);
          }}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"ลองเป็นเมนูนี้ดูไหม?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {randomDone ? (
                <div className="animate-bottom">
                  <h2>เมนู: {randomDone}</h2>
                  <p>ประเภทอาหาร: {foodCategory[randomType]}</p>
                </div>
              ) : (
                <div className="loader"></div>
              )}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                setOpen(false);
              }}
              autoFocus
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </main>
      <style jsx>
        {`
          .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 80px;
            height: 80px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            margin: 0 auto;
          }

          .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
          }

          @-webkit-keyframes animatebottom {
            from {
              bottom: -100px;
              opacity: 0;
            }
            to {
              bottom: 0px;
              opacity: 1;
            }
          }

          @keyframes animatebottom {
            from {
              bottom: -100px;
              opacity: 0;
            }
            to {
              bottom: 0;
              opacity: 1;
            }
          }

          /* Safari */
          @-webkit-keyframes spin {
            0% {
              -webkit-transform: rotate(0deg);
            }
            100% {
              -webkit-transform: rotate(360deg);
            }
          }

          @keyframes spin {
            0% {
              transform: rotate(0deg);
            }
            100% {
              transform: rotate(360deg);
            }
          }
        `}
      </style>
    </div>
  );
};

export default randomByBasicMenu;
