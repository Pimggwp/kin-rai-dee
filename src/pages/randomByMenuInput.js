import React, { useState, useEffect } from "react";
import Head from "next/head";
import { FaArrowLeft } from "react-icons/fa";
import Link from "next/link";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
  TextField,
  Card,
} from "@mui/material";

const randomByMenuInput = () => {
  const [foodList, setFoodList] = useState([]);
  const [open, setOpen] = useState(false);
  const [randomDone, setRandomDone] = useState("");
  const [value, setValue] = useState("");

  useEffect(() => {
    if (open === false) {
      setRandomDone("");
    }
  }, [open]);

  function randomInList() {
    setOpen(true);
    const random = Math.floor(Math.random() * foodList.length);
    setTimeout(() => {
      setRandomDone(foodList[random]);
    }, 1000);
  }

  function addToList() {
    if (value) {
      setFoodList((foodList) => [...foodList, value]);
      setValue("");
    }
  }

  return (
    <div
      style={{
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Head>
        <title>Random!</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main style={{ width: "100%" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <h1 style={{ marginBottom: 5, marginRight: 16, cursor: "pointer" }}>
            <Link href="/">
              <FaArrowLeft />
            </Link>
          </h1>
          <h1 className="title">
            สุ่มจาก<span style={{ color: "#F79D19" }}>เมนูที่คิดได้</span>
          </h1>
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            width: "100%",
            height: "calc(100vh - 110px)",
            flexDirection: "column",
          }}
        >
          <h3>
            {" "}
            มาเป็นแก๊งค์ ใครคิดอะไรออก แต่ไม่รู้จะเลือกอะไรดี
            หรือมีไอเดียที่อยากกินเต็มไปหมด ตัดสินใจไม่ได้
            กรอกเมนูแล้วสุ่มได้เลย{" "}
          </h3>
          <Box
            sx={{
              width: 500,
              maxWidth: "100%",
              display: "flex",
            }}
          >
            <TextField
              fullWidth
              label="กินอะไรดี?"
              id="fullWidth"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              onKeyDown={(ev) => {
                if (ev.key === "Enter") {
                  addToList();
                  ev.preventDefault();
                }
              }}
            />
            <Button
              style={{ marginLeft: 4 }}
              variant="text"
              onClick={() => addToList()}
            >
              Add
            </Button>
          </Box>
          <Box
            sx={{
              width: 500,
              maxWidth: "100%",
              display: "flex",
            }}
          >
            <Card
              variant="outlined"
              style={{
                width: "100%",
                padding: "10px",
                marginTop: "16px",
                minHeight: "calc(100vh - 390px)",
                maxHeight: "calc(100vh - 390px)",
                overflow: "scroll",
              }}
            >
              {foodList.map((el) => (
                <>
                  <span>{el}</span>
                  <br />
                </>
              ))}
            </Card>
          </Box>
          <div style={{ marginTop: 16 }}>
            <Button
              variant="contained"
              disabled={foodList.length === 0}
              onClick={() => randomInList()}
            >
              สุ่มเลย!
            </Button>
            <Button
              style={{ marginLeft: 8 }}
              variant="contained"
              disabled={foodList.length === 0}
              onClick={() => setFoodList([])}
            >
              Clear รายการ
            </Button>
          </div>
        </div>
        <Dialog
          open={open}
          onClose={() => {
            setOpen(false);
          }}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"เมนูที่สุ่มได้คือ.."}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {randomDone ? (
                <div className="animate-bottom">
                  <h2>เมนู: {randomDone}</h2>
                </div>
              ) : (
                <div className="loader"></div>
              )}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                setOpen(false);
              }}
              autoFocus
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </main>
      <style jsx>
        {`
          .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 80px;
            height: 80px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            margin: 0 auto;
          }

          .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
          }

          @-webkit-keyframes animatebottom {
            from {
              bottom: -100px;
              opacity: 0;
            }
            to {
              bottom: 0px;
              opacity: 1;
            }
          }

          @keyframes animatebottom {
            from {
              bottom: -100px;
              opacity: 0;
            }
            to {
              bottom: 0;
              opacity: 1;
            }
          }

          /* Safari */
          @-webkit-keyframes spin {
            0% {
              -webkit-transform: rotate(0deg);
            }
            100% {
              -webkit-transform: rotate(360deg);
            }
          }

          @keyframes spin {
            0% {
              transform: rotate(0deg);
            }
            100% {
              transform: rotate(360deg);
            }
          }
        `}
      </style>
    </div>
  );
};

export default randomByMenuInput;
